<?php
/* template head */
if (class_exists('Dwoo\Plugins\Functions\PluginInclude')===false)
	$this->getLoader()->loadPlugin('PluginInclude');
/* end template head */ ob_start(); /* template body */ ;
'';// checking for modification in file:Assets/partials/base.tpl
if (!("1511940544" == filemtime('Assets/partials/base.tpl'))) { ob_end_clean(); return false; };?><!DOCTYPE html>
<html>
<head>
	<title>Bảng xếp hạng</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="/Assets/jquery/jquery.min.js"></script>
	<script src="/Assets/js/bootstrap.min.js"></script>
	<script src="/Assets/js/script.js"></script>
	<script src="/Assets/js/bootstrap-rating.js"></script>

	<link rel="stylesheet" type="text/css" href="/Assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap-rating.css">

</head>
<body>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/navbar.tpl", null, null, null, '_root', null));?>
	  <?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/banner.tpl", null, null, null, '_root', null));?>
	<div class="trends-list">
  <div class="container">
      <?php 
$_fh0_data = (isset($this->scope["list"]) ? $this->scope["list"] : null);
$this->globals["foreach"]["trends"] = array
(
	"index"		=> 0,
);
$_fh0_glob =& $this->globals["foreach"]["trends"];
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['coffee'])
	{
/* -- foreach start output */
?>
    <div class="trend-item row">
      <div class="col-sm-2">
        <div class="rank"><?php echo $this->globals["foreach"]["trends"]["index"]+1;?></div>
      </div>
      <div class="col-sm-10">
        <p class="name"><a href="/store/<?php echo $this->scope["coffee"]["id"];?>"><?php echo $this->scope["coffee"]["name"];?></a></p>
        <p class="info">
          <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
          <?php echo $this->scope["coffee"]["address"];?>
          <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
          <?php echo $this->scope["coffee"]["timeOperation"];?>
        </p>
        <p class="rating">
          <span class="glyphicon glyphicon-heart" aria-hidden="true"></span> <?php echo $this->scope["coffee"]["score"];?>/5 &emsp;
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $this->scope["coffee"]["total_vote"];?> thành viên đã đánh giá
        </p>
        <p class="description">
          <?php echo $this->scope["coffee"]["description"];?>
        </p>
      </div>
    </div>
    <hr />
    <?php 
/* -- foreach end output */
		$_fh0_glob["index"]+=1;
	}
}?>
  </div>
</div>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/footer.tpl", null, null, null, '_root', null));?>
</body>
</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>