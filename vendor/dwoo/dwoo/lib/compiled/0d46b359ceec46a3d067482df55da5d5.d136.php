<?php
/* template head */
if (class_exists('Dwoo\Plugins\Functions\PluginInclude')===false)
	$this->getLoader()->loadPlugin('PluginInclude');
/* end template head */ ob_start(); /* template body */ ;
'';// checking for modification in file:Assets/partials/base.tpl
if (!("1511940544" == filemtime('Assets/partials/base.tpl'))) { ob_end_clean(); return false; };?><!DOCTYPE html>
<html>
<head>
	<title>Result</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="/Assets/jquery/jquery.min.js"></script>
	<script src="/Assets/js/bootstrap.min.js"></script>
	<script src="/Assets/js/script.js"></script>
	<script src="/Assets/js/bootstrap-rating.js"></script>

	<link rel="stylesheet" type="text/css" href="/Assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap-rating.css">

</head>
<body>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/navbar.tpl", null, null, null, '_root', null));?>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/slideshow.tpl", null, null, null, '_root', null));?>

<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/searchbox.tpl", null, null, null, '_root', null));?>

	   
<div class="search-list">
  <h3 class="search-header">Kết quả: có <?php echo $this->scope["count"];?> kết quả được tìm thấy</h3>
  <hr>
  <div class="container">
    <?php 
$_fh0_data = (isset($this->scope["list"]) ? $this->scope["list"] : null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['result'])
	{
/* -- foreach start output */
?>
    <div class="promo-item row">
      <div class="col-xs-4 photo">
        <img src="/image/<?php echo $this->scope["result"]["id"];?>" alt="<?php echo $this->scope["result"]["name"];?>">
      </div>
      <div class="col-xs-8 detail">
        <a class="cf-name" href="/store/<?php echo $this->scope["result"]["id"];?>">
          <h3><?php echo $this->scope["result"]["name"];?></h3>
          <p class="address"><?php echo $this->scope["result"]["address"];?></p>
        </a>
        <p><?php echo $this->scope["result"]["description"];?></p>
      </div>
    </div>
    <hr>
    <?php 
/* -- foreach end output */
	}
}?>
  </div>
</div>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/footer.tpl", null, null, null, '_root', null));?>
</body>
</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>