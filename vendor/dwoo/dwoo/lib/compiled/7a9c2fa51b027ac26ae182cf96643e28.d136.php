<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div id="showtable">
	<h2 class="text-center">Danh sách thành viên</h2>
	<div style="overflow-x:auto; padding-top: 20px; padding-bottom: 20px; justify-content: center; display: flex;">
		<table class="table-bordered table-hover table-responsive">
			<tr><th>Tài khoản</th><th>Mật khẩu</th><th>Tên người dùng</th><th>Email</th><th>Hành động</th></tr>
			<?php if ((isset($this->scope["tablemode"]) ? $this->scope["tablemode"] : null) == 1) {
?>
			<?php 
$_fh0_data = (isset($this->scope["list"]) ? $this->scope["list"] : null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['member'])
	{
/* -- foreach start output */
?>
			<tr>
				<td><?php echo $this->scope["member"]["username"];?></td>
				<td><?php echo $this->scope["member"]["password"];?></td>
				<td><?php echo $this->scope["member"]["name"];?></td>
				<td><?php echo $this->scope["member"]["email"];?></td>
				<td>
					<button class="btn btn-default" onclick=" EditMemberInfo( <?php echo $this->scope["member"]["id"];?> ) ">
						<span id="icon-edit-shop-info" class="glyphicon glyphicon-pencil"></span>
					</button>
					<button class="btn btn-default" onclick=" DeleteMemberInfo( <?php echo $this->scope["member"]["id"];?> ) ">
						<span id="icon-edit-shop-info" class="glyphicon glyphicon-remove"></span>
					</button>
				</td>
			</tr>
			<?php 
/* -- foreach end output */
	}
}?>
			<?php 
}
else {
?>
			<?php 
$_fh1_data = (isset($this->scope["list"]) ? $this->scope["list"] : null);
if ($this->isTraversable($_fh1_data) == true)
{
	foreach ($_fh1_data as $this->scope['member'])
	{
/* -- foreach start output */
?>
			<tr>
				<?php if ((isset($this->scope["member"]["id"]) ? $this->scope["member"]["id"]:null) == (isset($this->scope["edit"]) ? $this->scope["edit"] : null)) {
?>
				<td><?php echo $this->scope["member"]["username"];?></td>
				<td><?php echo $this->scope["member"]["password"];?></td>
				<td><input type="text" id="edit-name" value="<?php echo $this->scope["member"]["name"];?>"></td>
				<td><input type="text" id="edit-email" value="<?php echo $this->scope["member"]["email"];?>"></td>
				<td><button class="btn btn-default" onclick="UpdateMemberInfo( <?php echo $this->scope["member"]["id"];?> )">
					<span id="icon-edit-shop-info" class="glyphicon glyphicon-ok"></span>
				</button>
				<?php 
}
else {
?>
				<td><?php echo $this->scope["member"]["username"];?></td><td><?php echo $this->scope["member"]["password"];?></td>
				<td><?php echo $this->scope["member"]["name"];?></td><td><?php echo $this->scope["member"]["email"];?></td>
				<td><button class="btn btn-default" onclick="EditMemberInfo( <?php echo $this->scope["member"]["id"];?> )">
					<span id="icon-edit-shop-info" class="glyphicon glyphicon-pencil"></span>
				</button>
				<?php 
}?>
				<button class="btn btn-default" onclick="DeleteMemberInfo( <?php echo $this->scope["member"]["id"];?> )">
					<span id="icon-edit-shop-info" class="glyphicon glyphicon-remove"></span>
				</button>
			</td>
		</tr>
		<?php 
/* -- foreach end output */
	}
}?>
		<?php 
}?>
		<tr>
			<td><input type="text" id="new-username"></td>
			<td><input type="text" id="new-pwd"></td>
			<td><input type="text" id="new-name"></td>
			<td><input type="text" id="new-email"></td>
			<td><button class="btn btn-default" onclick="InsertNewMember()">Thêm thành viên mới</button></td>
		</tr>
	</table>
</div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>