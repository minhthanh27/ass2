<?php
/* template head */
if (class_exists('Dwoo\Plugins\Functions\PluginInclude')===false)
	$this->getLoader()->loadPlugin('PluginInclude');
/* end template head */ ob_start(); /* template body */ ;
'';// checking for modification in file:Assets/partials/base_form.tpl
if (!("1511940544" == filemtime('Assets/partials/base_form.tpl'))) { ob_end_clean(); return false; };
'';// checking for modification in file:Assets/partials/base.tpl
if (!("1511940544" == filemtime('Assets/partials/base.tpl'))) { ob_end_clean(); return false; };?><!DOCTYPE html>
<html>
<head>
	<title>Cập nhật thông tin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="/Assets/jquery/jquery.min.js"></script>
	<script src="/Assets/js/bootstrap.min.js"></script>
	<script src="/Assets/js/script.js"></script>
	<script src="/Assets/js/bootstrap-rating.js"></script>

	<link rel="stylesheet" type="text/css" href="/Assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap-rating.css">

</head>
<body>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/navbar.tpl", null, null, null, '_root', null));?>
	  <?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/banner.tpl", null, null, null, '_root', null));?>
	<div class="container">
  <h2 class="text-center">Cập nhật thông tin</h2>
  	<?php if (((isset($this->scope["error"]) ? $this->scope["error"] : null) !== null)) {
?>
		<?php if ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 2) {
?>
			<div class="alert alert-danger">
				<strong>Thất bại!</strong> Email không hợp lệ!
			</div>
		<?php 
}
elseif ((isset($this->scope["error"]) ? $this->scope["error"] : null) == 1) {
?>
			<div class="alert alert-success">
				<strong>Thành công!</strong> Cập nhật thông tin cá nhân thành công!
			</div>
		<?php 
}
elseif ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 3) {
?>
			<div class="alert alert-success">
				<strong>Thất bại!</strong> Email không được để trống!
			</div>
		<?php 
}
elseif ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 1) {
?>
			<div class="alert alert-success">
				<strong>Thất bại!</strong> Tên người dùng không được để trống!
			</div>
		<?php 
}?>
	<?php 
}?>
  <form method="POST" action="" class="col-xs-6 col-xs-offset-3" style="padding-bottom: 20px;">
    	<div class="form-group">
      <label for="name">Tên của bạn:</label>
      <input type="text" class="form-control" id="name" value="<?php echo $this->scope["member"]["name"];?>" name="name" required>
    </div>
    
    
    
    	<div class="form-group">
      <label for="email">E-mail:</label>
      <input type="email" class="form-control" id="email" value="<?php echo $this->scope["member"]["email"];?>" name="email" required>
    </div>
    <div class="help-block with-errors"></div>
    
    <button type="submit" class="btn btn-success col-xs-6 col-xs-offset-3">Hoàn tất</button>
  </form>
</div>          
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/footer.tpl", null, null, null, '_root', null));?>
</body>
</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>