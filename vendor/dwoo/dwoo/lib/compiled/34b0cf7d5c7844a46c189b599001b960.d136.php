<?php
/* template head */
if (class_exists('Dwoo\Plugins\Functions\PluginInclude')===false)
	$this->getLoader()->loadPlugin('PluginInclude');
/* end template head */ ob_start(); /* template body */ ;
'';// checking for modification in file:Assets/partials/base_form.tpl
if (!("1511940544" == filemtime('Assets/partials/base_form.tpl'))) { ob_end_clean(); return false; };
'';// checking for modification in file:Assets/partials/base.tpl
if (!("1511940544" == filemtime('Assets/partials/base.tpl'))) { ob_end_clean(); return false; };?><!DOCTYPE html>
<html>
<head>
	<title>Đăng nhập</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="/Assets/jquery/jquery.min.js"></script>
	<script src="/Assets/js/bootstrap.min.js"></script>
	<script src="/Assets/js/script.js"></script>
	<script src="/Assets/js/bootstrap-rating.js"></script>

	<link rel="stylesheet" type="text/css" href="/Assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap-rating.css">

</head>
<body>
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/navbar.tpl", null, null, null, '_root', null));?>
	  <?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/banner.tpl", null, null, null, '_root', null));?>
	<div class="container">
  <h2 class="text-center">Đăng nhập</h2>
  	<?php if ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 3) {
?>
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không tồn tại!
		</div>
	<?php 
}
elseif ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 4) {
?>
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Sai mật khẩu!
		</div>
	<?php 
}
elseif ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 1) {
?>
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không được để trống!
		</div>
	<?php 
}
elseif ((isset($this->scope["error"]) ? $this->scope["error"] : null) == - 2) {
?>
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu không được để trống!
		</div>
	<?php 
}?>
  <form method="POST" action="" class="col-xs-6 col-xs-offset-3" style="padding-bottom: 20px;">
    
        <div class="form-group">
      <label for="name">Tên đăng nhập:</label>
      <input type="text" class="form-control" id="name" placeholder="Nhập tài khoản" name="username" required>
    </div>
        <div class="form-group">
      <label for="pwd">Mật khẩu:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Nhập mật khẩu" name="password" required>
    </div>
    
    
    	<div>
		<a class="btn btn-link" href="/forget">Quên mật khẩu?</a>
	</div>
    <button type="submit" class="btn btn-success col-xs-6 col-xs-offset-3">Đăng nhập</button>
  </form>
</div>          
	<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude', 
                        array("Assets/partials/footer.tpl", null, null, null, '_root', null));?>
</body>
</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>