<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="menu" id="menu">
  <div class="container">
    <p class="text-center">
      <?php echo $this->scope["coffee"]["slogan"];?>   
    </p>
    <h3 class="text-center">MENU</h3>
    <div class="show-items col-sm-6 col-sm-offset-3 col-xs-12 borderimg">
      <?php 
$_fh0_data = (isset($this->scope["list"]) ? $this->scope["list"] : null);
$this->globals["foreach"]["menulist"] = array
(
	"index"		=> 0,
);
$_fh0_glob =& $this->globals["foreach"]["menulist"];
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['menu'])
	{
/* -- foreach start output */
?>
      <div class="row line" id="row-menu">
        <div class="col-md-3 col-md-offset-3 col-xs-3 col-xs-offset-1">
          <p class="item" id="drinkname-<?php echo $this->globals["foreach"]["menulist"]["index"];?>"><?php echo $this->scope["menu"]["name"];?></p>
          <small id="drinkdescription-<?php echo $this->globals["foreach"]["menulist"]["index"];?>"><?php echo $this->scope["menu"]["description"];?></small>
        </div>
    
        <div class="col-md-2 col-md-offset-1 col-xs-2 col-xs-offset-2 text-right" id="drinkprice-<?php echo $this->globals["foreach"]["menulist"]["index"];?>">
          <?php echo sprintf('%d 000 VND',(isset($this->scope["menu"]["price"]) ? $this->scope["menu"]["price"]:null)/1000);?>
        </div>
        <?php if ((isset($this->scope["username"]) ? $this->scope["username"] : null) == 'admin') {
?>
        <div class="col-md-3 col-xs-3">
          <button type="button" class="btn btn-default btn-sm pull-left" 
                  onclick='DeleteDishRecord(<?php echo $this->scope["menu"]["id"];?>, <?php echo $this->scope["menu"]["store_id"];?>)'>
            <span class="glyphicon glyphicon-remove"></span>
          </button>
          <button type="button" class="btn btn-default btn-sm pull-left" id="btn-edit-drink-<?php echo $this->globals["foreach"]["menulist"]["index"];?>" onclick='EditDrink(<?php echo $this->globals["foreach"]["menulist"]["index"];?>,<?php echo $this->scope["menu"]["id"];?>)'>
            <span class="glyphicon glyphicon-pencil" id="icon-edit-drink-<?php echo $this->globals["foreach"]["menulist"]["index"];?>"></span>
          </button>
        </div>
        <?php 
}?>

      </div>
      <hr/>
      <?php 
/* -- foreach end output */
		$_fh0_glob["index"]+=1;
	}
}?>
      <?php if ((isset($this->scope["username"]) ? $this->scope["username"] : null) == 'admin') {
?>
      <form class="col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1 ">
        <div class="form-group row">
          <label for="new-dishname" class="col-2 col-xs-2 col-form-label">Tên món</label>
          <div class="col-10 col-xs-10">
            <input class="form-control" type="text" id="new-dishname" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="new-dishdescription" class="col-2 col-xs-2 col-form-label">Mô tả</label>
          <div class="col-10 col-xs-10"">
            <input class="form-control" type="text" id="new-dishdescription" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="new-dishprice" class="col-2 col-xs-2 col-form-label">Giá</label>
          <div class="col-10 col-xs-10"">
            <input class="form-control" type="number" id="new-dishprice">
          </div>
        </div>
        <div class="form-group row">
          <button type="button" class="btn btn-default btn-sm center-block" onclick="NewDishRecord(<?php echo $this->scope["coffee"]["id"];?>)">
            <span class="glyphicon glyphicon-plus"></span> Thêm món
          </button>
        </div>
      </form>
      <?php 
}?>
    </div>
  </div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>