<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div id="carousel-feedback" class="carousel slide" data-ride="carousel" data-interval="4000">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php 
$_fh1_data = (isset($this->scope["fblist"]) ? $this->scope["fblist"] : null);
$this->globals["foreach"]["qlist"] = array
(
	"index"		=> 0,
	"first"		=> null,
	"total"		=> $this->count($_fh1_data),
);
$_fh1_glob =& $this->globals["foreach"]["qlist"];
if ($this->isTraversable($_fh1_data) == true)
{
	foreach ($_fh1_data as $this->scope['feedback'])
	{
		$_fh1_glob["first"] = (string) ($_fh1_glob["index"] === 0);
/* -- foreach start output */
?>
      <?php if ((isset($this->globals["foreach"]["qlist"]["index"]) ? $this->globals["foreach"]["qlist"]["index"]:null)%4 == 0) {
?>
        <?php if ((isset($this->globals["foreach"]["qlist"]["first"]) ? $this->globals["foreach"]["qlist"]["first"]:null)) {
?>
          <li data-target="#carousel-feedback" data-slide-to="0" class="active"></li>
        <?php 
}
else {
?>
          <li data-target="#carousel-feedback" data-slide-to=" <?php echo $this->globals["foreach"]["qlist"]["index"]/4;?> "></li>
        <?php 
}?>
      <?php 
}?>
    <?php 
/* -- foreach end output */
		$_fh1_glob["index"]+=1;
	}
}?>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner carousel-feedback-inner" role="listbox">
    <?php 
$_fh2_data = (isset($this->scope["fblist"]) ? $this->scope["fblist"] : null);
$this->globals["foreach"]["qlist"] = array
(
	"index"		=> 0,
	"first"		=> null,
	"total"		=> $this->count($_fh2_data),
);
$_fh2_glob =& $this->globals["foreach"]["qlist"];
if ($this->isTraversable($_fh2_data) == true)
{
	foreach ($_fh2_data as $this->scope['feedback'])
	{
		$_fh2_glob["first"] = (string) ($_fh2_glob["index"] === 0);
/* -- foreach start output */
?>
    <?php if ((isset($this->globals["foreach"]["qlist"]["index"]) ? $this->globals["foreach"]["qlist"]["index"]:null)%4 == 0) {
?>
      <?php if ((isset($this->globals["foreach"]["qlist"]["first"]) ? $this->globals["foreach"]["qlist"]["first"]:null)) {
?>
    <div class="item active" >
      <?php 
}
else {
?>
      <div class="item" >
      <?php 
}?>

      <div class="row">
    <?php 
}?>
        <div class="col-md-6 feedback" id="slide-feedback">
          <div class="col-md-10">
          <blockquote>
            <p><?php echo $this->scope["feedback"]["content"];?></p>
            <footer><?php echo $this->scope["feedback"]["author"];?> đã nhận xét tại <cite title="Source Title"><?php echo $this->scope["feedback"]["source"];?></cite></footer>
          </blockquote>
          </div>
          <?php if ((isset($this->scope["username"]) ? $this->scope["username"] : null) == 'admin') {
?>
            <div class="col-md-2">
            <button type="button" class="btn btn-default btn-sm center-block" onclick='DeleteFeedbackRecord(<?php echo $this->scope["feedback"]["id"];?>,<?php echo $this->scope["feedback"]["store_id"];?>)'>
                <span class="glyphicon glyphicon-remove"></span>
            </button>
          </div>
          <?php 
}?>
        </div>
           <?php if (( (isset($this->globals["foreach"]["qlist"]["index"]) ? $this->globals["foreach"]["qlist"]["index"]:null)%4 == 3 ) || ( (isset($this->globals["foreach"]["qlist"]["index"]) ? $this->globals["foreach"]["qlist"]["index"]:null) == ( ((isset($this->globals["foreach"]["qlist"]["total"]) ? $this->globals["foreach"]["qlist"]["total"]:null) - 1) ) )) {
?>
      </div>
       
    </div>
    <?php 
}?>
    <?php 
/* -- foreach end output */
		$_fh2_glob["index"]+=1;
	}
}?>
  </div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>