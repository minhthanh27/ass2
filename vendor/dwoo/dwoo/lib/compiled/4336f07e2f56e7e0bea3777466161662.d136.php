<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><nav class="navbar-header navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand bar-item" href="/">Coffee Finder</a>
    </div>

    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav-menu nav navbar-nav navbar-right">
        <li>
          <a class="bar-item" href="/">Trang chủ</a>
        </li>
        <li>
          <a class="bar-item" href="/trends">Bảng xếp hạng</a>
        </li>
        <li>
          <a class="bar-item" href="/promotion">Khuyến mãi</a>
        </li>
        <li>
          <a class="bar-item" href="/about-us">Về chúng tôi</a>
        </li>
        <?php if (((isset($this->scope["username"]) ? $this->scope["username"] : null) !== null)) {
?>
          <li>
            <a class="bar-item" href="/user"><?php echo $this->scope["username"];?></a>
          </li>
          <li>
            <a class="bar-item" href="/logout">Đăng xuất</a>
          </li>
        <?php 
}
else {
?>
          <li>
            <a class="bar-item" href="/signup">Đăng ký</a>
          </li>
          <li>
            <a class="bar-item" href="/login">Đăng nhập</a>
          </li>
        <?php 
}?>
      </ul>
    </div>
  </div>
</nav><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>