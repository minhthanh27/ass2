<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="container">
  <h3>GỢI Ý</h3>
  <div class="row">
    <?php 
$_fh0_data = (isset($this->scope["list"]) ? $this->scope["list"] : null);
if ($this->isTraversable($_fh0_data) == true)
{
	foreach ($_fh0_data as $this->scope['coffee'])
	{
/* -- foreach start output */
?>
    <div class="col-md-4 col-sm-6">
      <div class="thumbnail cf-box">
        <img class="img-responsive" src="/image/<?php echo $this->scope["coffee"]["id"];?>" alt="<?php echo $this->scope["coffee"]["name"];?>">
        <?php if ((isset($this->scope["username"]) ? $this->scope["username"] : null) == 'admin') {
?>
        <div class="img-overlay">
          <button class="btn btn-default btn-sm" onclick="DeleteShop(<?php echo $this->scope["coffee"]["id"];?>)">
            <span id="icon-edit-shop-info" class="glyphicon glyphicon-remove"></span>
          </button>
        </div>
        <?php 
}?>
        <div class="detail">
          <a class="cf-name" href="/store/<?php echo $this->scope["coffee"]["id"];?>">
            <span>
              <?php echo $this->scope["coffee"]["name"];?>
            </span>
          </a>
          <p class="address"><?php echo $this->scope["coffee"]["address"];?></p>
        </div>
      </div>
    </div>
    <?php 
/* -- foreach end output */
	}
}?>
  </div>
</div><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>