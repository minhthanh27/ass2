<?php
// Create a connection, once only.
$config = array(
    'driver'    => 'mysql', // Db driver
    'host'      => 'localhost',
    'database'  => 'coffee-finder',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8', // Optional
    'collation' => 'utf8_unicode_ci', // Optional
    'prefix'    => '', // Table prefix, optional
    'options'   => array( // PDO constructor options, optional
        PDO::ATTR_TIMEOUT => 5,
        PDO::ATTR_EMULATE_PREPARES => false,
    ),
);

new \Pixie\Connection('mysql', $config, 'DB');

class ConnectDatabase
{
    /**
    * @brief Get one coffee shop info by id
    */

    public function GetStoreInfoById($id)
    {
        $info = DB::table('coffee')->find($id);
        $result = json_decode(json_encode($info), true);
        return $result;
    }

    /**
    * @brief Get all coffee shop info in database
    */

    public function GetStoreInfoAll()
    {
        $info = DB::table('coffee')->select('*');
        $result = $info->get();
        $result = json_decode(json_encode($result), true);
        return $result;
    }

    /**
    * @brief Insert new store record to database
    */

    public function InsertStoreInfo($info)
    {
        return DB::table('coffee')->insert($info);
    }

    /**
    * @brief Update store info
    */

    public function UpdateStoreInfo($id, $info)
    {
        $result = DB::table('coffee')->where('id', $id)->update($info);
        //TODO: handle NULL
    }

    /**
    * @brief insert new member info to database
    */

    public function InsertMemberInfo($info)
    {
        $result = DB::table('member')->where('username', '=', $info['username']);
        if ($result->get())
        {
            return false;
        }
        $insertId = DB::table('member')->insert($info);
        return true;
    }

    /**
    * @brief Get all member info
    */

    public function GetMemberInfoAll()
    {
        $list = DB::table('member')->select('*');
        $list = $list->get();
        $list = json_decode(json_encode($list), true);
        return $list;
    }

    /**
    * @brief Get member info by id provied
    */

    public function GetMemberInfoById($id)
    {
        $member = DB::table('member')->find($id);
        $member = json_decode(json_encode($member), true);
        return $member;
    }

    /**
    * @brief Update member name and email by id provied
    */

    public function UpdateMemberInfoById($id, $name, $email)
    {
        $sql = 'update member set name=\''.$name.'\', email=\''.$email.'\' where id=\''.$id.'\'';
        $result = DB::query($sql);
    }

    /**
    * @brief Update member password
    * @return -1 if current password is wrong
    * @return 0 if success
    */

    public function UpdateMemberPasswordById($id, $curpwd, $newpwd)
    {
        $member = DB::table('member')->find($id);
        $member = json_decode(json_encode($member), true);
        if ($member['password'] == $curpwd)
        {
            $sql = 'update member set password=\''.$newpwd.'\' where id=\''.$id.'\'';
            $result = DB::query($sql);
            return 0;
        }
        else
        {
            return -1;
        }
    }

    /**
    * @brief Renew member password by id, username and email provied
    */

    public function RenewMemberPasswordById($id, $usermail, $email)
    {
        //$sql = 'update member set name=\''.$name.'\', email=\''.$email.'\' where id=\''.$id.'\'';
        //$result = DB::query($sql);
    }

    /**
    * @brief Get one coffee store menu by id
    */

    public function GetShopMenuById($id)
    {
        $menu = DB::table('menu')->findAll('store_id', $id);
        $menu = json_decode(json_encode($menu), true);
        return $menu;
    }

    /**
    * @brief Get one coffee store feedback by id
    */

    public function GetShopFeedbackById($id)
    {
        $feedback = DB::table('feedback')->findAll('store_id', $id);
        $feedback = json_decode(json_encode($feedback), true);
        $count = count($feedback);
        $i = 0;
        while ($i < $count)
        {
            $member = DB::table('member')->find($feedback[$i]['author_id']);
            $member = json_decode(json_encode($member), true);
            if ($member)
            {
                $feedback[$i]['author'] = $member['username'];
            }
            else
            {
                $feedback[$i]['author'] = 'Amonyous';
            }
            $i++;
        }
        return $feedback;
    }

    /**
    * @brief Get record that matches location and name
    */

    public function GetShopSearchResult($shopname, $location)
    {
        $sql = 'select * from coffee where (name like "%'.$shopname.'%" 
        or name like "'.$shopname.'%" 
        or name like "%'.$shopname.'") 
        and (address like "%'.$location.' %"
        or address like "'.$location.' %"
        or address like "%'.$location.'")';
        $search = DB::query($sql);
        $search = json_decode(json_encode($search->get()), true);
        return $search;
    }

    /**
    * @brief Get all coffee shop info and order by rank
    */

    public function GetTopStoreInfo()
    {
        $result = DB::query('select * from coffee order by score desc');
        $result = json_decode(json_encode($result->get()), true);
        return $result;
    }

    /**
    * @brief Check authorize
    * @return -3 if username is not exist
    * @return -4 if password is wrong
    * @return 1 if success
    */

    public function Authorize($username, $password)
    {
        $code = 1;
        $result = DB::table('member')->where('username', '=', $username);
        $result = $result->get();
        if ($result)
        {
            $result = json_decode(json_encode($result), true);
            if ($result[0]['password'] != $password)
            {
                $code = -4;
            }
        }
        else
        {
            $code = -3;
        }
        return $code;
    }

    /**
    * @brief Insert new feedback
    * @param feedback[in] new feedback
    * @return id of new record
    */

    public function InsertShopFeedback($feedback)
    {
        $insertID = DB::table('feedback')->insert($feedback);
        return $insertID;
    }

    /**
    * @brief Get user id by username
    * @param username[in] username to map
    * @return user
    */

    public function GetUserId($username)
    {
        $user = DB::table('member')->where('username', '=', $username);
        $user = $user->get();
        if ($user)
        {
            $user = json_decode(json_encode($user), true);
            return $user;
        }
        return false;
    }

    /**
    * @brief Delete member
    * @param id[in] member id
    */

    public function DeleteMember($id)
    {
        DB::table('member')->where('id', '=', $id)->delete();
    }

    /**
    * @brief Check member info valid, used when new member is added by admin
    * @param idfo[in] member info
    * @return ....
    */

    /*public function AddNewMemberInfo($info)
    {
        $validator = new Validator\LIVR( [
            'username'  =>  'required',
            'password'  =>  ['required', ['min_length' => 8]],
            'email'     =>  'email',
            'name'      =>  'required'
        ] );
        $validData = $validator->validate($info);
        if ($validData)
        {
            unset($validData['password2']);
            $result = $db->InsertMemberInfo($validData);
            if ($result)
            {
                //Successfull
                $data->assign('error', '0');
                goto show;
            }
            else
            {   
                //Username is duplicated
                $data->assign('error', '-5');
                goto show;
            }
        }
        else 
        {
            $error = $validator->getErrors();
            if (isset($error['name']))
            {
                //Empty name
                $data->assign('error', '-6');
                goto show;
            }
            if (isset($error['username']))
            {
                //Empty username
                $data->assign('error', '-1');
                goto show;
            }
            if (isset($error['password']))
            {
                if ($error['password'] == 'REQUIRED')
                {
                    //Empty password
                    $data->assign('error', '-2');
                    goto show;
                }
                else
                {
                    //Short password
                    $data->assign('error', '-4');
                    goto show;
                }
            }
            if (isset($error['email']))
            {
                if ($error['email'] == 'REQUIRED')
                {
                    //Empty email
                    $data->assign('error', '-7');
                    goto show;
                }
                else
                {
                    //Invalid email
                    $data->assign('error', '-8');
                    goto show;
                }
            }
        }
    */
        
    /**
    * @brief save shop cover image
    * @param id[in] shop id
    * @param name[in] to get image 
    */

    public function StoreImages($id, $name)
    {
        $imageUploader = new ImageUploader();
        $imageUploader->setPath("Assets/images");   
        $imageUploader->setMinFileSize(1);                           
        $imageUploader->setMaxFileSize(5000000);    
        $imageUploader->upload($_FILES[$name], $id);
    }

    /**
    * @brief show shop cover image
    * @param id[in] shop id
    */

    public function ShowImages($id)
    {
        $imageUploader = new ImageUploader();
        $imageUploader->setPath("Assets/images");
        $imageUploader->serve($id);
    }

    /**
    * @brief send email
    * @param reciever[in] receive email
    * @param message[in] email content
    * @return true if send email success
    */

    public function SendMail($reciever, $message)
    {
        $mail = new PHPMailer\PHPMailer\PHPMailer(false);
        try {
            //Server settings
            $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'coffeefinder2017@gmail.com';                 // SMTP username
            $mail->Password = 'mailaanhem';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('coffeefinder2017@gmail.com', 'CoffeeFinder');
            $mail->addAddress($reciever);     // Add a recipient

            //Content
            $mail->Subject = '[Coffe finder] Here is your password';
            $mail->Body    = $message;

            $mail->send();
        } 
        catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
    * @brief Check username and password in database
    * @return true if valid (belong to exist member)
    */

    public function VerifyBeforeSendPwd($username, $email)
    {
        $user = DB::table('member')->where('username', '=', $username);
        $user = $user->get();
        if ($user)
        {
            $user = json_decode(json_encode($user), true);
            if ($user[0]['email'] == $email)
            {
                return $user[0]['password'];
            }
            else
            {
                return -2;
            }
        }
        return -1;
    }

    /**
    * @brief add new drink in shop menu
    * @param info[in] drink info
    * @return new drink id in database
    */

    public function InsertDishesInfo($info)
    {
        $insertId = DB::table('menu')->insert($info);
        return $insertId;
    }

    /**
    * @brief delete member feedback, only admin 
    * @param id[in] feedback id
    */

    public function DeleteFeedback($id) 
    {
        DB::table('feedback')->where('id', '=', $id)->delete();
    }

    /**
    * @brief add new drink in shop menu
    * @param vote[in] new vote info
    */

    public function InsertNewVote($vote)
    {
        DB::table('ranking')->insert($vote);
        $search = DB::query('update coffee set total_vote = total_vote + 1 where id = \''.$vote[0]['shop_id'].'\'');
        $search = DB::query('update coffee 
            set score = (((score*(total_vote-1))+'.$vote[0]['score'].')/total_vote) 
            where id = '.$vote[0]['shop_id'].'');
    }

    /**
    * @brief check if current member voted this shop before
    * @param member[in] member id
    * @param shop[id] shop id
    * @return -1 if no vote, score if voted
    */

    public function CheckVote($member, $shop)
    {
        $sql = 'select * from ranking where member_id = '.$member.' 
        and shop_id = '.$shop.'';
        $search = DB::query($sql);
        $search = $search->get();
        $search = json_decode(json_encode($search), true);
        if ($search)
        {
            return $search[0]['score'];
        }
        return -1;
    }

    /**
    * @brief Delete one menu item by id
    * @return none
    */

    public function DeleteMenuItem($id)
    {
        DB::table('menu')->where('id', '=', $id)->delete();
    }

    public function UpdateMenuInfo($id, $info)
    {
        $result = DB::table('menu')->where('id', $id)->update($info);
    }

    public function DeleteStore($id)
    {
        DB::table('coffee')->where('id', '=', $id)->delete();
    }
}
?>