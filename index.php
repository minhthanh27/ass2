<?php

require './vendor/autoload.php';

// Load DB
require './db.php';

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
	$r->addRoute('GET', '/', 'index');
	$r->addRoute(['GET', 'POST'], '/{controller:.+}', 'controller');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
	$uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

if ($routeInfo[0] == FastRoute\Dispatcher::NOT_FOUND) {
	// Render 404 page
	echo '404 Error';
} else if ($routeInfo[0] == FastRoute\Dispatcher::METHOD_NOT_ALLOWED) {
	// Render 405 page
	echo '405 Error';
} else if ($routeInfo[0] == FastRoute\Dispatcher::FOUND) {
	// Router here
	$handlerName = $routeInfo[1];
	$params = $routeInfo[2];
	if ($handlerName == 'index') {
		include('./Controlers/index.php');
	} else if ($handlerName == 'controller') {
		$result = explode("/", $params['controller']);
		if (file_exists('./Controlers/'.$result[0].'.php')) {
			require('./Controlers/'.$result[0].'.php');
		} else {
			echo 'Opps. This controller doesn exist';
		}
	}
	} else {
		// Unknow error
		echo 'Unknow error';
	}

?>