<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

$db = new ConnectDatabase();

$result = $db->GetStoreInfoAll();

$method = $_SERVER['REQUEST_METHOD'];

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

if ($method == 'POST')
{
	if (isset($_POST['op']))
	{
		$op = $_POST['op'];
		switch ($op) {
			case '1':
			{
				$db->DeleteStore($_POST['id']);
				$data->assign('list', $db->GetStoreInfoAll());
				echo $view->get('./Assets/partials/index-shops.tpl', $data);
				exit;
			}
		}
	}
}

$data->assign('list', $result);

echo $view->get('./View/index.tpl', $data);

?>