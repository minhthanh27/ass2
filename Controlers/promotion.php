<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$db = new ConnectDatabase();

$result = $db->GetStoreInfoAll();

$data->assign('list', $result);

echo $view->get('./View/promotion.tpl', $data);

?>