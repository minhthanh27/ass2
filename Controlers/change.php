<?php

$view = new Dwoo\Core();
$data = new Dwoo\Data();
$db = new ConnectDatabase();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$id = $db->GetUserId($_COOKIE['username'])[0]['id'];

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST')
{
	$validator = new Validator\LIVR( [
		'curpwd'	=> 	'required',
		'password'  =>  ['required', [ 'min_length' => 8 ]],
		'password2' =>  [ 'equal_to_field' => 'password' ]
	] );
	$validData = $validator->validate($_POST);
	if ($validData)
	{
		$result = $db->UpdateMemberPasswordById($id, $validData['curpwd'], $validData['password']);
		$data->assign('error' , $result);
		goto show;
	}
	else
	{
		$error = $validator->getErrors();
		if (isset($error['password']))
		{
			if ($error['password'] == 'REQUIRED')
			{
				//Empty password
				$data->assign('error', '-4');
			}
			else
			{
				//Too short password
				$data->assign('error', '-3');
			}
		}
		if (isset($error['password2']))
		{
			//Retype password error
			$data->assign('error', '-2');
			
		}
		if (isset($error['curpwd']))
		{
			//Empty current password
			$data->assign('error', '-5');
		}
		goto show;
	}
}
else if ($method == 'GET')
{
	goto show;
}

show:
	$member = $db->GetMemberInfoById($id);
	$data->assign('member', $member);
	echo $view->get('./View/change.tpl', $data);
?>