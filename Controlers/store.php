<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();	

$db = new ConnectDatabase();

$id = explode("/", $_SERVER['REQUEST_URI'])[2];

$method = $_SERVER['REQUEST_METHOD'];


if (isset($_COOKIE['username']))
{
	$user = $_COOKIE['username'];
	if ($user)
	{
		$data->assign('username', $user);
		$data->assign('score', $db->CheckVote($db->GetUserId($user)[0]['id'], $id));
	}
}



if ($method == "POST")
{
	if (isset($_FILES['edit-photo']))
	{
		$db->StoreImages($id, "edit-photo");
		$data->assign('coffee', $db->GetStoreInfoById($id));
		$data->assign('list', $db->GetShopMenuById($id));
		$data->assign('fblist', $db->GetShopFeedbackById($id));
		echo $view->get('./View/store.tpl', $data);
		exit;
	}
	if (isset($_POST['op'])){
		$op = $_POST['op'];
		switch ($op) 
		{
			case '1':  // Add new drink
			{
				$name = $_POST['name'];
				$description = $_POST['description'];
				$price = $_POST['price'];
				$info = array(
					'name'		=> $name,
					'description' => $description,
					'price' => $price,
					'store_id' => $id
				);
				$res = $db->InsertDishesInfo($info);
				$data->assign('list', $db->GetShopMenuById($id));
				$data->assign('coffee', $db->GetStoreInfoById($id));
				echo $view->get('./Assets/partials/menu.tpl', $data);
				exit;
			}
			case '2':  // Update drink
			{
				$id_item = $_POST['id'];
				$name = $_POST['name'];
				$description = $_POST['description'];
				$price = $_POST['price'];
				$info = array(
					'id' => $id_item,		
					'name'		=> $name,
					'description' => $description,
					'price' => $price
				);
				$db->UpdateMenuInfo($id_item, $info);
				$data->assign('list', $db->GetShopMenuById($id));
				$data->assign('coffee', $db->GetStoreInfoById($id));
				echo $view->get('./Assets/partials/menu.tpl', $data);
				exit;
			}
			
			case '3':  // Change cover image
			{
				$db->StoreImages($id, "editphoto");
				$data->assign('coffee', $db->GetStoreInfoById($id));
				echo $view->get('./Assets/partials/shop-banner.tpl', $data);
				exit;
			}

			case '4': // Delete menu item
			{
				$id_item = $_POST['id'];
				$db->DeleteMenuItem($id_item);
				$data->assign('list', $db->GetShopMenuById($id));
				$data->assign('coffee', $db->GetStoreInfoById($id));
				echo $view->get('./Assets/partials/menu.tpl', $data);
				exit;
			}
			
			case '5': // Delete member feedback
			{
				$id_feedback = $_POST['id'];
				$db->DeleteFeedback($id_feedback);
				$data->assign('fblist', $db->GetShopFeedbackById($id));
				echo $view->get('./Assets/partials/feedback.tpl', $data);
				exit;
			}

			case '6': // Voting
			{
				$info = array ([
					'member_id'		=> $db->GetUserId($user)[0]['id'],
					'shop_id' 		=> $id,
					'score' 		=> $_POST['score']
				]);
				$db->InsertNewVote($info);
				exit;
			}

			case '7': //update Shop Info
			{
				$name = $_POST['name'];
				$timeOperation = $_POST['timeOperation'];
				$phone = $_POST['phone'];
				$address = $_POST['address'];
				$info = array (
					'name' => $name,
					'timeOperation' => $timeOperation,
					'phone' => $phone,
					'address' => $address
				);
				$db->UpdateStoreInfo($id, $info);
				$data->assign('coffee', $db->GetStoreInfoById($id));
				echo $view->get('./Assets/partials/shop-banner.tpl', $data);
				exit;
			}
			//default: break;
			
		}
	}
	
	if (isset($_POST['comment']))
	{
		if (!isset($_POST['op']) && ($_POST['comment'] == NULL))
		{
			$error = -1;
			$data->assign('error', $error);
		}
		else
		{
			$feedback = array([
				'content'		=> $_POST['comment'],
				'author_id'		=> $db->GetUserId($user)[0]['id'],
				'store_id'		=> $id
			]);
			$db->InsertShopFeedback($feedback);
			$error = 1;
			$data->assign('error', $error);
		}
	}
}

$store = $db->GetStoreInfoById($id);

$menu = $db->GetShopMenuById($id);

$feedback = $db->GetShopFeedbackById($id);

$data->assign('coffee', $store);

$data->assign('list', $menu);

$data->assign('fblist', $feedback);

echo $view->get('./View/store.tpl', $data);

?>