<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

$db = new ConnectDatabase();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$id = $db->GetUserId($_COOKIE['username'])[0]['id'];

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST')
{
	$validator = new Validator\LIVR( [
		'name'	 => 'required',
		'email'  => ['required', 'email']
	] );
	$validData = $validator->validate($_POST);
	if ($validData)
	{
		$db->UpdateMemberInfoById($id, $_POST['name'], $_POST['email']);
		$data->assign('error' , '1');
		goto show;
	}
	else
	{
		$error = $validator->getErrors();
		if (isset($error['email']))
		{
			if ($error['email'] == 'REQUIRED')
			{
				//Empty email
				$data->assign('error', '-3');
			}
			else
			{
				//Wrong email
				$data->assign('error', '-2');
			}
		}
		if (isset($error['name']))
		{
			//Empty name
			$data->assign('error', '-1');
		}
		goto show;
	}
}
else if ($method == 'GET')
{
	goto show;
}

show:
	$member = $db->GetMemberInfoById($id);
	$data->assign('member', $member);
	echo $view->get('./View/edit.tpl', $data);
?>