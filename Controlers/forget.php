<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

$db = new ConnectDatabase();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST')
{
	$validator = new Validator\LIVR( [
		'username'	=> 'required',
		'email'  => ['required', 'email']
	] );
	$validData = $validator->validate($_POST);
	if ($validData)
	{
		$result = $db->VerifyBeforeSendPwd($validData['username'], $validData['email']);
		$data->assign('error', $result);
		if ($result != '-2' && $result != '-1')
		{
			$db->SendMail($validData['email'], $result);
			$data->assign('error', '1');
		}
	}
	else
	{
		$error = $validator->getErrors();
		if (isset($error['username']))
		{
			//Empty username
			$data->assign('error', '-3');
		}
		if (isset($error['email']))
		{
			//Empty password
			$data->assign('error', '-4');
		}
	}
}
else
{
	$data->assign('error', '0');
}

echo $view->get('./View/forget.tpl', $data);

?>