<?php
// Create Template instance
$view = new Dwoo\Core();

$data = new Dwoo\Data();

require './vendor/validator/livr/lib/Validator/LIVR.php';

Validator\LIVR::defaultAutoTrim(true);

$db = new ConnectDatabase();

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST')
{
	$validator = new Validator\LIVR( [
		'name'		=> 'required',
		'username'	=> 'required',
		'password'  => ['required', ['min_length' => 8]],
		'password2' => ['equal_to_field' => 'password'],
		'email'		=> ['required', 'email']
	] );
	$validData = $validator->validate($_POST);
	if ($validData)
	{
		unset($validData['password2']);
		$result = $db->InsertMemberInfo($validData);
		if ($result)
		{
			//Successfull
			$data->assign('error', '0');
			goto show;
		}
		else
		{	
			//Username is duplicated
			$data->assign('error', '-5');
			goto show;
		}
	}
	else 
	{
		$error = $validator->getErrors();
		if (isset($error['name']))
		{
			//Empty name
			$data->assign('error', '-6');
			goto show;
		}
		if (isset($error['username']))
		{
			//Empty username
			$data->assign('error', '-1');
			goto show;
		}
		if (isset($error['password']))
		{
			if ($error['password'] == 'REQUIRED')
			{
				//Empty password
				$data->assign('error', '-2');
				goto show;
			}
			else
			{
				//Short password
				$data->assign('error', '-4');
				goto show;
			}
		}
		if (isset($error['password2']))
		{
			if ($error['password2'] == 'REQUIRED')
			{
				//Empty password
				$data->assign('error', '-2');
				goto show;
			}
			else
			{
				//Not match password
				$data->assign('error', '-3');
				goto show;
			}
		}
		if (isset($error['email']))
		{
			if ($error['email'] == 'REQUIRED')
			{
				//Empty email
				$data->assign('error', '-7');
				goto show;
			}
			else
			{
				//Invalid email
				$data->assign('error', '-8');
				goto show;
			}
		}
	}
}
else if ($method == 'GET')
{
	$data->assign('error', '1');
	goto show;
}

show:
	echo $view->get('./View/signup.tpl', $data);
?>