<?php
// Create Template instance
$view = new Dwoo\Core();

$data = new Dwoo\Data();

$db = new ConnectDatabase();

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST')
{
	$validator = new Validator\LIVR( [
		'username'	=> 'required',
		'password'  => 'required'
	] );
	$validData = $validator->validate($_POST);
	if ($validData)
	{
		$result = $db->Authorize($validData['username'], $validData['password']);
		if ($result == 1)
		{
			$cookie_name = "username";
			$cookie_value = $_POST['username'];
			setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 30 days
			header("location: /");
		}
		$data->assign('error', $result);
	}
	else
	{
		$error = $validator->getErrors();
		if (isset($error['username']))
		{
			//Empty username
			$data->assign('error', '-1');
		}
		if (isset($error['password']))
		{
			//Empty password
			$data->assign('error', '-2');
		}
	}
	goto show;
}
else if ($method == 'GET')
{
	$data->assign('error', '1');
	goto show;
}

show:
	echo $view->get('./View/login.tpl', $data);

?>