<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$db = new ConnectDatabase();

$result = $db->GetTopStoreInfo();

$data->assign('list', $result);

echo $view->get('./View/trends.tpl', $data);
?>