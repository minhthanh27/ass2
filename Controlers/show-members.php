<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

$db = new ConnectDatabase();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$data->assign('list', $db->GetMemberInfoAll());

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST')
{	
	switch ($_POST['op']) {
        case '1': 
        {
            $username = $_POST['username'];
			$email = $_POST['email'];
			$pwd = $_POST['pwd'];
			$name = $_POST['name'];
            /*if (ValidateRecord($id, $name, $year))
            {
                NewRecord($id, $name, $year);
            }*/
            $info = array(
            	'username' 	=> $username, 
            	'email'		=> $email,
            	'password'	=> $pwd,
            	'name'		=> $name,
            );
            $db->InsertMemberInfo($info);
            $data->assign('list', $db->GetMemberInfoAll());
            echo $view->get('./Assets/partials/member-lists.tpl', $data);
            exit;
        }
        case '2': // update, after edit 
        {  
            $name = $_POST['name'];
            $email = $_POST['email'];
            $id = $_POST['id'];
            /*if (ValidateRecord($id, $name, $year))
            {
                EditRecord($id, $name, $year);
            }*/
            $db->UpdateMemberInfoById($id, $name, $email);
            $data->assign('list', $db->GetMemberInfoAll());
            echo $view->get('./Assets/partials/member-lists.tpl', $data);
            exit;
        }
        case '3': // delete
        {
            $id = $_POST['id'];
            $db->DeleteMember($id);
            $data->assign('list', $db->GetMemberInfoAll());
            echo $view->get('./Assets/partials/member-lists.tpl', $data);
            exit;
        }
        case '4': // editting
        {
            $data->assign('tablemode', 0); //tablemode = 0 -> editting
            $data->assign('edit', $_POST['id']);
            echo $view->get('./Assets/partials/member-lists.tpl', $data);
            exit;
        }
	}
}
else if ($method == 'GET')
{
	$data->assign('tablemode', 1); //tablemode = 1 -> showing
	goto show;
}

show:
    echo $view->get('./View/show-members.tpl', $data);
?>