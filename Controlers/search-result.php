<?php

$view = new Dwoo\Core();

$data = new Dwoo\Data();

$db = new ConnectDatabase();

if (isset($_COOKIE['username']))
{
	$data->assign('username', $_COOKIE['username']);
}

$location = $_POST['location'];
$shopname = $_POST['shop-name'];


$search = $db->GetShopSearchResult($shopname, $location);

$count = count($search);

$data->assign('list', $search);

$data->assign('count', $count);

echo $view->get('./View/search-result.tpl', $data);
?>