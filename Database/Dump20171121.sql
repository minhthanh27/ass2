-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: coffee-finder
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coffee`
--

DROP TABLE IF EXISTS `coffee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coffee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT 'Không có thông tin',
  `address` varchar(255) NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `timeOperation` varchar(45) DEFAULT 'Không có thông tin',
  `description` varchar(255) DEFAULT 'Không có thông tin',
  `slogan` varchar(45) DEFAULT 'Không có thông tin',
  `promo` varchar(255) DEFAULT 'Không có thông tin',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `total_vote` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coffee`
--

LOCK TABLES `coffee` WRITE;
/*!40000 ALTER TABLE `coffee` DISABLE KEYS */;
INSERT INTO `coffee` VALUES (7,'121','','2121',18,'','','','',4,2),(8,'The Coffee House','012344567','aaaa Quận 1 TPHCM',18,'9h-21h','Lorem','Lorem','Lorem',3,2);
/*!40000 ALTER TABLE `coffee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(45) NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `source` varchar(45) DEFAULT 'Tại quán',
  `store_id` int(10) unsigned NOT NULL,
  `author` varchar(45) DEFAULT 'Amonyous',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (4,'Good',1,'quán',1,NULL),(5,'Bad',2,'quán',2,NULL),(6,'GGWP',5,'quán',2,NULL),(10,'GGGG',1,'nhà',2,NULL),(11,'GOODDDD',2,'Facebook',2,NULL),(12,'BEST',5,'Bách Khoa',2,NULL),(13,'GGGGG',1,'nhà',2,NULL),(14,'Hey',11,'Tại quán',1,'Amonyous'),(16,'Ngon',11,'Tại quán',2,'Amonyous'),(17,'Best',11,'Tại quán',1,'Amonyous'),(18,'BAdddd',11,'Tại quán',3,'Amonyous'),(19,'So expensive!!!!!',11,'Tại quán',4,'Amonyous'),(21,'ngon <3',14,'Tại quán',3,'Amonyous'),(22,'aaaa',11,'Tại quán',3,'Amonyous'),(23,'Thạnh đã dạo chơi nơi đây',15,'Tại quán',1,'Amonyous'),(24,'aaa',12,'Tại quán',2,'Amonyous'),(25,'1',11,'Tại quán',3,'Amonyous'),(26,'2',11,'Tại quán',3,'Amonyous'),(27,'3',11,'Tại quán',3,'Amonyous'),(28,'hi',18,'Tại quán',1,'Amonyous');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `join_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_time` timestamp NULL DEFAULT NULL,
  `name` varchar(45) NOT NULL DEFAULT 'Amonyous',
  `email` varchar(45) NOT NULL DEFAULT 'Unknown',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (17,'admin2','12345678222','2017-11-13 12:11:38',NULL,'chi123444','chi96@mail.com.vn'),(18,'admin','12345678','2017-11-15 02:41:09',NULL,'1','chi96@mail.com'),(19,'admin3','121212121212','2017-11-13 12:22:12',NULL,'chi','chi96@mail.com'),(20,'testing','12345678','2017-11-15 01:53:57',NULL,'tester','test@example.com'),(21,'testemail','12345678','2017-11-15 08:20:37',NULL,'Chi','trancamchi96@gmail.com');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (2,1,'Cà phê đen','Black Coffee','40000'),(3,1,'Cà phê sữa','Capuchino','50000'),(4,2,'Cà phê đen','Lorem','40000'),(5,2,'Trà sữa','Loram','30000'),(6,7,'Capuchino','Cà phê','100000'),(7,7,'Cà phê','Cà phê đen','500000');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ranking`
--

DROP TABLE IF EXISTS `ranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `shop_id` int(10) unsigned NOT NULL,
  `score` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ranking`
--

LOCK TABLES `ranking` WRITE;
/*!40000 ALTER TABLE `ranking` DISABLE KEYS */;
INSERT INTO `ranking` VALUES (3,21,7,3),(4,21,8,5),(5,18,7,5),(6,18,8,1);
/*!40000 ALTER TABLE `ranking` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21 19:25:09
