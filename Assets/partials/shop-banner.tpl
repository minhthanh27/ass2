<div class="shop-info" style="background-image: url('/image/{$coffee.id}');" id="shop-banner">
	{if $username == 'admin'}
	<form method="POST" action="" id="photo-form" enctype="multipart/form-data" style="padding-bottom: 20px; padding-top: 20px;">
		<div class="form-group">
			<label title="Cập nhật ảnh bìa" class="btn btn-default btn-lg button-edit">
				<span class="glyphicon glyphicon-pencil"></span> &nbsp; Cập nhật ảnh bìa
				<input name="edit-photo" type="file" class="hidden" onchange="this.form.submit()">
			</label>
		</div>
	</form>
	{/if}
	<div class="shop-detail">
		{if $username == 'admin'}
		<button id="btn-edit-shop-info" type="button" class="btn btn-default btn-sm pull-right"  onclick='EditShopInfo({$coffee.id})'>
			<span id="icon-edit-shop-info" class="glyphicon glyphicon-pencil"></span>
		</button>
		{/if}
		<h1 id="coffee-name">{$coffee.name}</h1>
		<p class="avaiable-time" id="avaiable-time">{$coffee.timeOperation}</p>
		<p class="phone" id="phone">{$coffee.phone}</p>
		<p class="address" id="address">{$coffee.address}</p>
	</div>
</div>
