<div id="showtable">
	<h2 class="text-center">Danh sách thành viên</h2>
	<div style="overflow-x:auto; padding-top: 20px; padding-bottom: 20px; justify-content: center; display: flex;">
		<table class="table-bordered table-hover table-responsive">
			<tr><th>Tài khoản</th><th>Mật khẩu</th><th>Tên người dùng</th><th>Email</th><th>Hành động</th></tr>
			{if $tablemode == 1}
			{foreach $list member}
			<tr>
				<td>{$member['username']}</td>
				<td>{$member['password']}</td>
				<td>{$member['name']}</td>
				<td>{$member['email']}</td>
				<td>
					<button class="btn btn-default" onclick=" EditMemberInfo( {$member['id'] } ) ">
						<span id="icon-edit-shop-info" class="glyphicon glyphicon-pencil"></span>
					</button>
					<button class="btn btn-default" onclick=" DeleteMemberInfo( {$member['id'] } ) ">
						<span id="icon-edit-shop-info" class="glyphicon glyphicon-remove"></span>
					</button>
				</td>
			</tr>
			{/foreach}
			{else}
			{foreach $list member}
			<tr>
				{if $member['id'] == $edit}
				<td>{$member['username']}</td>
				<td>{$member['password']}</td>
				<td><input type="text" id="edit-name" value="{$member['name']}"></td>
				<td><input type="text" id="edit-email" value="{$member['email']}"></td>
				<td><button class="btn btn-default" onclick="UpdateMemberInfo( {$member['id']} )">
					<span id="icon-edit-shop-info" class="glyphicon glyphicon-ok"></span>
				</button>
				{else}
				<td>{$member['username']}</td><td>{$member['password']}</td>
				<td>{$member['name']}</td><td>{$member['email']}</td>
				<td><button class="btn btn-default" onclick="EditMemberInfo( {$member['id']} )">
					<span id="icon-edit-shop-info" class="glyphicon glyphicon-pencil"></span>
				</button>
				{/if}
				<button class="btn btn-default" onclick="DeleteMemberInfo( {$member['id']} )">
					<span id="icon-edit-shop-info" class="glyphicon glyphicon-remove"></span>
				</button>
			</td>
		</tr>
		{/foreach}
		{/if}
		<tr>
			<td><input type="text" id="new-username"></td>
			<td><input type="text" id="new-pwd"></td>
			<td><input type="text" id="new-name"></td>
			<td><input type="text" id="new-email"></td>
			<td><button class="btn btn-default" onclick="InsertNewMember()">Thêm thành viên mới</button></td>
		</tr>
	</table>
</div>
</div>