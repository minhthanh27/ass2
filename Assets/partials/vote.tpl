<div class="text-center" id="vote-box">
	{if isset($score)}
	{if $score == -1}
	<label for="score" class="control-label">
		<h2>Đánh giá của bạn</h2>
	</label>
	<div onclick="Voting()">
		<input type="hidden" id="score" name="score" class="rating" data-min="0" data-max="5" data-step="1">
	</div>
	{else}
	<label for="show-score" class="control-label">
		<h2>Đánh giá của bạn</h2>
	</label>
	<div onclick="Voting()">
		<input type="hidden" id="show-score" class="rating" value='{$score}' disabled="disabled">
	</div>
	{/if}
	{/if}
</div>