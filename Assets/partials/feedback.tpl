<div id="carousel-feedback" class="carousel slide" data-ride="carousel" data-interval="4000">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    {foreach $fblist feedback name="qlist"}
      {if $.foreach.qlist.index % 4 == 0}
        {if $.foreach.qlist.first}
          <li data-target="#carousel-feedback" data-slide-to="0" class="active"></li>
        {else}
          <li data-target="#carousel-feedback" data-slide-to=" {$.foreach.qlist.index / 4} "></li>
        {/if}
      {/if}
    {/foreach}
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner carousel-feedback-inner" role="listbox">
    {foreach $fblist feedback name="qlist"}
    {if $.foreach.qlist.index % 4 == 0}
      {if $.foreach.qlist.first}
    <div class="item active" >
      {else}
      <div class="item" >
      {/if}

      <div class="row">
    {/if}
        <div class="col-md-6 feedback" id="slide-feedback">
          <div class="col-md-10">
          <blockquote>
            <p>{$feedback.content}</p>
            <footer>{$feedback.author} đã nhận xét tại <cite title="Source Title">{$feedback.source}</cite></footer>
          </blockquote>
          </div>
          {if $username == 'admin'}
            <div class="col-md-2">
            <button type="button" class="btn btn-default btn-sm center-block" onclick='DeleteFeedbackRecord({$feedback['id']},{$feedback['store_id']})'>
                <span class="glyphicon glyphicon-remove"></span>
            </button>
          </div>
          {/if}
        </div>
           {if ($.foreach.qlist.index % 4 == 3)|| ($.foreach.qlist.index == ($.foreach.qlist.total-1))}
      </div>
       
    </div>
    {/if}
    {/foreach}
  </div>
</div>