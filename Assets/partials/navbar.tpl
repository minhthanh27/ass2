<nav class="navbar-header navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand bar-item" href="/">Coffee Finder</a>
    </div>

    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav-menu nav navbar-nav navbar-right">
        <li>
          <a class="bar-item" href="/">Trang chủ</a>
        </li>
        <li>
          <a class="bar-item" href="/trends">Bảng xếp hạng</a>
        </li>
        <li>
          <a class="bar-item" href="/promotion">Khuyến mãi</a>
        </li>
        <li>
          <a class="bar-item" href="/about-us">Về chúng tôi</a>
        </li>
        {if isset($username)}
          <li>
            <a class="bar-item" href="/user">{$username}</a>
          </li>
          <li>
            <a class="bar-item" href="/logout">Đăng xuất</a>
          </li>
        {else}
          <li>
            <a class="bar-item" href="/signup">Đăng ký</a>
          </li>
          <li>
            <a class="bar-item" href="/login">Đăng nhập</a>
          </li>
        {/if}
      </ul>
    </div>
  </div>
</nav>