<div class="header-search">
  <div class="row">
    <form method="POST" action="/search-result" id="search-box">
      <div class="col-sm-12 hidden-xs">
        <h2 class="text-center">Tìm ngay quán <span class="hightligh">coffee</span> gần bạn</h2>
      </div>
      <div class="col-sm-6">
        <input type="text" class="form-control input-lg" name="shop-name" placeholder="Search for...">
      </div>
      <div class="col-sm-4">
        <select class="form-control input-lg" name="location" form="search-box">
          <option selected value="">Chọn khu vực</option>
          <option value="Quận 1">Quận 1</option>
          <option value="Quận 2">Quận 2</option>
          <option value="Quận 3">Quận 3</option>
          <option value="Quận 4">Quận 4</option>
          <option value="Quận 5">Quận 5</option>
          <option value="Quận 6">Quận 6</option>
          <option value="Quận 7">Quận 7</option>
          <option value="Quận 8">Quận 8</option>
          <option value="Quận 9">Quận 9</option>
          <option value="Quận 10">Quận 10</option>
          <option value="Quận 11">Quận 11</option>
          <option value="Quận 12">Quận 12</option>
          <option value="Bình Thạnh">Bình Thạnh</option>
          <option value="Thủ Đức">Thủ Đức</option>
          <option value="Bình Tân">Bình Tân</option>
          <option value="Tân Bình">Tân Bình</option>
          <option value="Bình Chánh">Bình Chánh</option>
          <option value="Củ Chi">Củ Chi</option>
          <option value="Phú Nhuận">Phú Nhuận</option>
          <option value="Gò Vấp">Gò Vấp</option>
          <option value="Hóc Môn">Hóc Môn</option>
          <option value="Tân Phú">Tân Phú</option>
          <option value="Nhà Bè">Nhà Bè</option>
          <option value="Cần Giờ">Cần Giờ</option>
        </select>
      </div>
      <div class="col-sm-2">
        <input type="submit" value="Tìm ngay" class="btn btn-info"></a>
      </div>
    </form>
  </div>
</div>