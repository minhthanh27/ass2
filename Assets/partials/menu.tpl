<div class="menu" id="menu">
  <div class="container">
    <p class="text-center">
      {$coffee.slogan}   
    </p>
    <h3 class="text-center">MENU</h3>
    <div class="show-items col-sm-6 col-sm-offset-3 col-xs-12 borderimg">
      {foreach $list menu name="menulist"}
      <div class="row line" id="row-menu">
        <div class="col-md-3 col-md-offset-3 col-xs-3 col-xs-offset-1">
          <p class="item" id="drinkname-{$.foreach.menulist.index}">{$menu.name}</p>
          <small id="drinkdescription-{$.foreach.menulist.index}">{$menu.description}</small>
        </div>
    
        <div class="col-md-2 col-md-offset-1 col-xs-2 col-xs-offset-2 text-right" id="drinkprice-{$.foreach.menulist.index}">
          {string_format($menu.price / 1000, '%d 000 VND')}
        </div>
        {if $username == 'admin'}
        <div class="col-md-3 col-xs-3">
          <button type="button" class="btn btn-default btn-sm pull-left" 
                  onclick='DeleteDishRecord({$menu.id}, {$menu.store_id})'>
            <span class="glyphicon glyphicon-remove"></span>
          </button>
          <button type="button" class="btn btn-default btn-sm pull-left" id="btn-edit-drink-{$.foreach.menulist.index}" onclick='EditDrink({$.foreach.menulist.index},{$menu['id']})'>
            <span class="glyphicon glyphicon-pencil" id="icon-edit-drink-{$.foreach.menulist.index}"></span>
          </button>
        </div>
        {/if}

      </div>
      <hr/>
      {/foreach}
      {if $username == 'admin'}
      <form class="col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1 ">
        <div class="form-group row">
          <label for="new-dishname" class="col-2 col-xs-2 col-form-label">Tên món</label>
          <div class="col-10 col-xs-10">
            <input class="form-control" type="text" id="new-dishname" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="new-dishdescription" class="col-2 col-xs-2 col-form-label">Mô tả</label>
          <div class="col-10 col-xs-10"">
            <input class="form-control" type="text" id="new-dishdescription" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="new-dishprice" class="col-2 col-xs-2 col-form-label">Giá</label>
          <div class="col-10 col-xs-10"">
            <input class="form-control" type="number" id="new-dishprice">
          </div>
        </div>
        <div class="form-group row">
          <button type="button" class="btn btn-default btn-sm center-block" onclick="NewDishRecord({$coffee.id})">
            <span class="glyphicon glyphicon-plus"></span> Thêm món
          </button>
        </div>
      </form>
      {/if}
    </div>
  </div>
</div>