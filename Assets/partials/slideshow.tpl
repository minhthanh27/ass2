<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="hidden-xs item active">
      <img src="Assets/images/slide-1.png" alt="...">
    </div>
    <div class="hidden-xs item">
      <img src="Assets/images/slide-2.png" alt="...">
    </div>
    <div class="hidden-xs item">
      <img src="Assets/images/slide-3.jpg" alt="...">
    </div>
  </div>
</div>