
<form method="POST" action="" class="col-sm-6 col-sm-offset-3 col-xs-12" enctype="multipart/form-data" style="padding-bottom: 20px; padding-top: 20px;">
    <div class="form-group">
      <label for="name">Tên quán:</label>
      <input type="text" class="form-control" id="name" placeholder="Nhập tên quán" name="name" required>
    </div>
    <div class="form-group">
      <label for="phone">Số điện thoại:</label>
      <input type="text" class="form-control" id="phone" placeholder="Nhập số điện thoại" name="phone">
    </div>
    <div class="form-group">
        <label for="add">Địa chỉ:</label>
        <input type="text" class="form-control" id="add" placeholder="Nhập địa chỉ" name="address" required>
      </div>
   
    <div class="form-group">
        <label for="timeO">Giờ hoạt động:</label>
        <input type="text" class="form-control" id="timeO" placeholder="Nhập giờ hoạt động" name="timeOperation">
    </div>
    <div class="form-group">
        <label for="des">Giới thiệu:</label>
        <textarea class="form-control" rows="10" placeholder="Nhập nội dung giới thiệu..." name="description"></textarea>
    </div>
    <div class="form-group">
        <label for="slogan">Slogan:</label>
        <input type="text" class="form-control" id="slogan" placeholder="Nhập slogan" name="slogan">
    </div>
    <div class="form-group">
        <label for="promo">Khuyến mại:</label>
        <input type="text" class="form-control" id="promo" placeholder="Nhập thông tin khuyến mại" name="promo">
    </div>
     <div class="form-group">
        <label for="photo1">Ảnh:</label>
        <input type="file" class="form-control" id="photo1" name="photo"  required>
    </div>
    <div class="centerbtn">
        <button type="submit" class="btn btn-success">Thêm quán</button>
    </div>
    
</form>
