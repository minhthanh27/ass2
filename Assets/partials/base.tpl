<!DOCTYPE html>
<html>
<head>
	<title>{block "title"}My site name{/block}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="/Assets/jquery/jquery.min.js"></script>
	<script src="/Assets/js/bootstrap.min.js"></script>
	<script src="/Assets/js/script.js"></script>
	<script src="/Assets/js/bootstrap-rating.js"></script>

	<link rel="stylesheet" type="text/css" href="/Assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap-rating.css">

</head>
<body>
	{include "Assets/partials/navbar.tpl"}
	{block "slideshow"}
	{block "searchbox"}{/block}
	{/block}
	{block "content"}My content{/block}
	{include "Assets/partials/footer.tpl"}
</body>
</html>