{extends "Assets/partials/base.tpl"}
{block "slideshow"}
  {include "Assets/partials/banner.tpl"}
{/block}
{block "content"}
<div class="container">
  <h2 class="text-center">{block "title"}{/block}</h2>
  {block "noti"}{/block}
  <form method="POST" action="" class="col-xs-6 col-xs-offset-3" style="padding-bottom: 20px;">
    {block "name"}{/block}
    {block "username"}{/block}
    {block "password"}{/block}
    {block "retype-password"}{/block}
    {block "email"}{/block}
    {block "forget-password"}{/block}
    <button type="submit" class="btn btn-success col-xs-6 col-xs-offset-3">{block "button-value"}{/block}</button>
  </form>
</div>          
{/block}