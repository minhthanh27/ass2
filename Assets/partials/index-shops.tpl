<div class="container">
  <h3>GỢI Ý</h3>
  <div class="row">
    {foreach $list coffee}
    <div class="col-md-4 col-sm-6">
      <div class="thumbnail cf-box">
        <img class="img-responsive" src="/image/{$coffee.id}" alt="{$coffee.name}">
        {if $username == 'admin'}
        <div class="img-overlay">
          <button class="btn btn-default btn-sm" onclick="DeleteShop({$coffee.id})">
            <span id="icon-edit-shop-info" class="glyphicon glyphicon-remove"></span>
          </button>
        </div>
        {/if}
        <div class="detail">
          <a class="cf-name" href="/store/{$coffee.id}">
            <span>
              {$coffee.name}
            </span>
          </a>
          <p class="address">{$coffee.address}</p>
        </div>
      </div>
    </div>
    {/foreach}
  </div>
</div>