function EditMemberInfo(id)
{
	$.ajax({
		url: "/show-members",
		type: "post",
		data: { 
			op: "4", 
			id: id 
		},
		dataType: "html",
		success: function( data ){
			document.getElementById('showtable').innerHTML = data;
		}
	});
}

function DeleteMemberInfo(id)
{
	$.ajax({
		url: "/show-members",
		type: "post",
		data: { 
			op: "3", 
			id: id 
		},
		dataType: "html",
		success: function( data ){
			document.getElementById('showtable').innerHTML = data;
		}
	});
}


function UpdateMemberInfo(id)
{
	var name = document.getElementById("edit-name").value;
	var email = document.getElementById("edit-email").value;
	$.ajax({
		url: "/show-members",
		type: "post",
		data: { 
			op: "2", 
			id: id,
			name: name,
			email: email
		},
		dataType: "html",
		success: function( data ){
			document.getElementById('showtable').innerHTML = data;
		}
	});
}

function InsertNewMember()
{
	var username = document.getElementById("new-username").value;
	var email = document.getElementById("new-email").value;
	var pwd = document.getElementById("new-pwd").value;
	var name = document.getElementById("new-name").value;
	$.ajax({
		url: "/show-members",
		type: "post",
		data: { 
			op: "1", 
			username: username,
			email: email,
			pwd: pwd,
			name: name
		},
		dataType: "html",
		success: function( data ){
			document.getElementById("showtable").innerHTML = data;
		}
	});
}

function NewDishRecord(id_store)
{
	var name = document.getElementById("new-dishname").value;
	var description = document.getElementById("new-dishdescription").value;
	var price = document.getElementById("new-dishprice").value;
	$.ajax({
		url: "/store/"+id_store,
		type: "post",
		data: { 
			op: "1", 
			name: name,
			description: description,
			price: price
		},
		dataType: "html",
		success: function( data ){
			document.getElementById("menu").innerHTML = data;
		}
	});
}

function DeleteDishRecord(id_item, id_store)
{
	$.ajax({
		url: "/store/"+id_store,
		type: "post",
		data:{
			op: "4",
			id: id_item
		},
		dataType: "html",
		success: function( data ){
			document.getElementById("menu").innerHTML = data;
		}
	});
}

function DeleteFeedbackRecord(id_feedback, id_store)
{
	$.ajax({
		url: "/store/"+id_store,
		type: "post",
		data:{
			op: "5",
			id: id_feedback
		},
		dataType: "html",
		success: function( data ){
			document.getElementById("carousel-feedback").innerHTML = data;
		}
	});
}

function Voting()
{
	$("#score").attr("disabled", "disabled");
	var score = document.getElementById("score").value;
	$.ajax({
		url: window.location.pathname,
		type: "post",
		data:{
			op: "6",
			score: score
		},
		dataType: "html",
		success: function( data ){
			document.getElementById("carousel-feedback").innerHTML = data;
		}
	});
}

function EditDrink(id, id_item) 
{
	document.getElementById("drinkname-"+id).contentEditable = true;
	document.getElementById("drinkdescription-"+id).contentEditable = true;
	document.getElementById("drinkprice-"+id).contentEditable = true;
	$("#icon-edit-drink-"+id).addClass("glyphicon-ok");
	$("#icon-edit-drink-"+id).removeClass("glyphicon-pencil");
	$("#btn-edit-drink-"+id).removeAttr("onclick");
	$("#btn-edit-drink-"+id).attr("onClick", "UpdateDrink("+id+","+id_item+")");
}

function UpdateDrink(id, id_item) {
	document.getElementById("drinkname-"+id).contentEditable = false;
	document.getElementById("drinkdescription-"+id).contentEditable = false;
	document.getElementById("drinkprice-"+id).contentEditable = false;
	$("#icon-edit-drink-"+id).addClass("glyphicon-ok");
	$("#icon-edit-drink-"+id).removeClass("glyphicon-pencil");
	$("#btn-edit-drink-"+id).removeAttr("onclick");
	$("#btn-edit-drink-"+id).attr("onClick", "EditDrink()");
	var name = document.getElementById("drinkname-"+id).textContent; 
	var description = document.getElementById("drinkdescription-"+id).textContent;
	var price = document.getElementById("drinkprice-"+id).textContent;
	price = parseInt(price)*1000;
	$.ajax({
		url: window.location.pathname,
		type: "post",
		data: { 
			op: "2", 
			id: id_item,
			name: name,
			description: description,
			price: price
		},
		dataType: "html",
		success: function( data ){
			document.getElementById('menu').innerHTML = data;
		}
	});
}

function EditShopInfo(id)
{
	document.getElementById("coffee-name").contentEditable = true;
	document.getElementById("avaiable-time").contentEditable = true;
	document.getElementById("phone").contentEditable = true;
	document.getElementById("address").contentEditable = true;
	$("#icon-edit-shop-info").addClass("glyphicon-ok");
	$("#icon-edit-shop-info").removeClass("glyphicon-pencil");
	$("#btn-edit-shop-info").removeAttr("onclick");
	$("#btn-edit-shop-info").attr("onClick", "UpdateShopInfo()");
}

function UpdateShopInfo() {
	document.getElementById("coffee-name").contentEditable = false;
	document.getElementById("avaiable-time").contentEditable = false;
	document.getElementById("phone").contentEditable = false;
	document.getElementById("address").contentEditable = false;
	$("#icon-edit-shop-info").addClass("glyphicon-pencil");
	$("#icon-edit-shop-info").removeClass("glyphicon-ok");
	$("#btn-edit-shop-info").removeAttr("onclick");
	$("#btn-edit-shop-info").attr("onClick", "EditShopInfo(id)");
	var name = document.getElementById("coffee-name").textContent; 
	var timeOperation = document.getElementById("avaiable-time").textContent;
	var phone = document.getElementById("phone").textContent;
	var address = document.getElementById("address").textContent;
	$.ajax({
		url: window.location.pathname,
		type: "post",
		data: { 
			op: "7", 
			name: name,
			timeOperation: timeOperation,
			phone: phone,
			address: address
		},
		dataType: "html",
		success: function( data ){
			document.getElementById('shop-banner').innerHTML = data;
		}
	});
}

function DeleteShop(id)
{
	$.ajax({
		url: '/index',
		type: "post",
		data: { 
			op: "1", 
			id: id
		},
		dataType: "html",
		success: function( data ){
			document.getElementById('container').innerHTML = data;
		}
	});
}