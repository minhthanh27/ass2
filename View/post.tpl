{extends "Assets/partials/base.tpl"}
{block "title"}Thêm quán{/block}
{block "slideshow"}
  {include "Assets/partials/banner.tpl"}
{/block}
{block "content"}
<div class="container">
    <h2 class="text-center">{block "title"}Thêm quán{/block}</h2>
   	{include "Assets/partials/post-form.tpl"}
   	</div>
{/block}
