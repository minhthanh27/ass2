{extends "Assets/partials/base.tpl"}
{block "title"}Result{/block}
{block "slideshow"}
{include "Assets/partials/slideshow.tpl"}
{block "searchbox"}
{include "Assets/partials/searchbox.tpl"}
{/block}
{/block}
{block "content"}   
<div class="search-list">
  <h3 class="search-header">Kết quả: có {$count} kết quả được tìm thấy</h3>
  <hr>
  <div class="container">
    {foreach $list result}
    <div class="promo-item row">
      <div class="col-xs-4 photo">
        <img src="/image/{$result.id}" alt="{$result.name}">
      </div>
      <div class="col-xs-8 detail">
        <a class="cf-name" href="/store/{$result.id}">
          <h3>{$result.name}</h3>
          <p class="address">{$result.address}</p>
        </a>
        <p>{$result.description}</p>
      </div>
    </div>
    <hr>
    {/foreach}
  </div>
</div>
{/block}