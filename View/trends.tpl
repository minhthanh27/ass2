{extends "Assets/partials/base.tpl"}
{block "title"}Bảng xếp hạng{/block}
{block "slideshow"}
  {include "Assets/partials/banner.tpl"}
{/block}
{block "content"}
<div class="trends-list">
  <div class="container">
      {foreach $list coffee name="trends"}
    <div class="trend-item row">
      <div class="col-sm-2">
        <div class="rank">{$.foreach.trends.index + 1}</div>
      </div>
      <div class="col-sm-10">
        <p class="name"><a href="/store/{$coffee.id}">{$coffee.name}</a></p>
        <p class="info">
          <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
          {$coffee.address}
          <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
          {$coffee.timeOperation}
        </p>
        <p class="rating">
          <span class="glyphicon glyphicon-heart" aria-hidden="true"></span> {$coffee.score}/5 &emsp;
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {$coffee.total_vote} thành viên đã đánh giá
        </p>
        <p class="description">
          {$coffee.description}
        </p>
      </div>
    </div>
    <hr />
    {/foreach}
  </div>
</div>
{/block}