﻿{extends "Assets/partials/base.tpl"}
{block "title"}Về chúng tôi{/block}
{block "slideshow"}
  {include "Assets/partials/banner.tpl"}
{/block}
{block "content"}
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h1 class="text-center">Thành viên</h1>
      <div class="row">
        <div class="member col-sm-3 col-xs-12">
          <img title="Gấu của Nguyễn Duy Hải" src="Assets/images/member.jpg" alt="#">
          <h4>Nguyễn Quốc Bảo</h4>
        </div>
        <div class="member col-sm-3 col-xs-12">
          <img title="Gấu của Nguyễn Duy Hải" src="Assets/images/member.jpg" alt="#">
          <h4>Trần Cẩm Chi</h4>
        </div>
        <div class="member col-sm-3 col-xs-12">
          <img title="Gấu của Nguyễn Duy Hải" src="Assets/images/member.jpg" alt="#">
          <h4>Nguyễn Duy Hải</h4>
        </div>
        <div class="member col-sm-3 col-xs-12">
          <img title="Gấu của Nguyễn Duy Hải" src="Assets/images/member.jpg" alt="#">
          <h4>Lê Minh Thành</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="text-center">Địa chỉ liên hệ</h2>
      <iframe id="google-map" class="gg-map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7838.987261148588!2d106.659815!3d10.773454!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xef77cd47a1cc691e!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBCw6FjaCBraG9hIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2sin!4v1508251605442"  allowfullscreen></iframe>
    </div>
  </div>
</div>
{/block}