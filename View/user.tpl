{extends "Assets/partials/base.tpl"}
{block "title"}{$username}{/block}
{block "slideshow"}
  {include "Assets/partials/banner.tpl"}
{/block}
{block "content"}
    <div class="container">
       <div class="col-md-6 col-md-offset-3" style="padding-bottom: 20px;">
        <h1 class="text-center">Chức năng</h1>
        <a class="btn btn-info btn-lg btn-block" href="/post">Thêm quán của bạn</a>
        <a class="btn btn-info btn-lg btn-block" href="/edit">Cập nhật thông tin cá nhân</a>
        <a class="btn btn-info btn-lg btn-block" href="/change">Đổi mật khẩu</a>
        {if isset($username)}
            {if $username=='admin'}
                <a class="btn btn-info btn-lg btn-block" href="/show-members">Xem danh sách thành viên</a>
            {/if}
        {/if}
        <a class="btn btn-info btn-lg btn-block" href="/logout">Đăng xuất</a>
        </div>
    </div>
{/block}