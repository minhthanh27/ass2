{extends "Assets/partials/base_form.tpl"}
{block "title"}Đăng ký{/block}
{block "noti"}
	{if $error == 0}
		<div class="alert alert-success">
			<strong>Thành công!</strong> Đăng ký thành công!
		</div>
	{elseif $error == -1}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không được để trống!
		</div>
	{elseif $error == -2}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu không được để trống!
		</div>	
	{elseif $error == -3}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Nhập lại mật khẩu không khớp!
		</div>
	{elseif $error == -4}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu phải trên 8 ký tự!
		</div>
	{elseif $error == -5}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên đăng nhập đã tồn tại!
		</div>
	{elseif $error == -6}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên người dùng không được để trống!
		</div>
	{elseif $error == -7}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Email không được để trống!
		</div>		
	{elseif $error == -8}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Email không hợp lệ!
		</div>			
	{/if}
{/block}
{block "name"}
	<div class="form-group">
      <label for="name">Tên của bạn:</label>
      <input type="text" class="form-control" id="name" placeholder="Nhập tên" name="name" required>
    </div>
{/block}
{block "username"}
    <div class="form-group">
      <label for="name">Tên đăng nhập:</label>
      <input type="text" class="form-control" id="name" placeholder="Nhập tài khoản" name="username" required>
    </div>
{/block}
{block "password"}
    <div class="form-group">
      <label for="pwd">Mật khẩu:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Nhập mật khẩu" name="password" required>
    </div>
{/block}
{block "retype-password"}
<div class="form-group">
	<label for="pwd2">Nhập lại mật khẩu:</label>
	<input type="password" class="form-control" id="pwd2" placeholder="Nhập lại mật khẩu" name="password2" required>
</div>
{/block}
{block "email"}
	<div class="form-group">
      <label for="email">E-mail:</label>
      <input type="email" class="form-control" id="email" placeholder="Nhập email" name="email" required>
    </div>
    <div class="help-block with-errors"></div>
{/block}
{block "button-value"}Đăng ký{/block}