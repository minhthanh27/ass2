{extends "Assets/partials/base_form.tpl"}
{block "title"}Đăng nhập{/block}
{block "noti"}
	{if $error == -3}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không tồn tại!
		</div>
	{elseif $error == -4}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Sai mật khẩu!
		</div>
	{elseif $error == -1}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không được để trống!
		</div>
	{elseif $error == -2}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu không được để trống!
		</div>
	{/if}
{/block}
{block "button-value"}Đăng nhập{/block}
{block "username"}
    <div class="form-group">
      <label for="name">Tên đăng nhập:</label>
      <input type="text" class="form-control" id="name" placeholder="Nhập tài khoản" name="username" required>
    </div>
{/block}
{block "password"}
    <div class="form-group">
      <label for="pwd">Mật khẩu:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Nhập mật khẩu" name="password" required>
    </div>
{/block}
{block "forget-password"}
	<div>
		<a class="btn btn-link" href="/forget">Quên mật khẩu?</a>
	</div>
{/block}