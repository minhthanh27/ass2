{extends "Assets/partials/base_form.tpl"}
{block "title"}Cập nhật thông tin{/block}
{block "noti"}
	{if isset($error)}
		{if $error == -2}
			<div class="alert alert-danger">
				<strong>Thất bại!</strong> Email không hợp lệ!
			</div>
		{elseif $error == 1}
			<div class="alert alert-success">
				<strong>Thành công!</strong> Cập nhật thông tin cá nhân thành công!
			</div>
		{elseif $error == -3}
			<div class="alert alert-success">
				<strong>Thất bại!</strong> Email không được để trống!
			</div>
		{elseif $error == -1}
			<div class="alert alert-success">
				<strong>Thất bại!</strong> Tên người dùng không được để trống!
			</div>
		{/if}
	{/if}
{/block}
{block "button-value"}Hoàn tất{/block}
{block "name"}
	<div class="form-group">
      <label for="name">Tên của bạn:</label>
      <input type="text" class="form-control" id="name" value="{$member.name}" name="name" required>
    </div>
{/block}
{block "email"}
	<div class="form-group">
      <label for="email">E-mail:</label>
      <input type="email" class="form-control" id="email" value="{$member.email}" name="email" required>
    </div>
    <div class="help-block with-errors"></div>
{/block}
