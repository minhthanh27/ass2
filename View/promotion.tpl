{extends "Assets/partials/base.tpl"}
{block "title"}Khuyến mãi{/block}
{block "slideshow"}
  {include "Assets/partials/banner.tpl"}
{/block}
{block "content"}
<div class="promotion-list">
  <h3 class="text-center">Coffee Finder mang lại cho các bác các khuyến mãi hấp dẫn nhất</h3>
  <hr>
  <div class="container">
    {foreach $list promo name="promolist"}
    <div class="promo-item row">
      {if $.foreach.promolist.index % 2 == 0 }
      <div class="col-xs-4 photo">
        <img src="/image/{$promo.id}" alt="{$promo.name}">
      </div>
      {/if}
      <div class="col-xs-8 detail">
        <a class="cf-name" href="/store/{$promo.id}">{$promo.name}</a>
        <p>
          {$promo.promo}
        </p>
      </div>
      {if $.foreach.promolist.index % 2 == 1 }
      <div class="col-xs-4 photo">
        <img src="/image/{$promo.id}" alt="{$promo.name}">
      </div>
      {/if}
    </div>
    <hr>
    {/foreach}
  </div>
</div>
{/block}