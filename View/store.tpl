{extends "Assets/partials/base.tpl"}
{block "title"}{$coffee.name}{/block}

{block "content"}
{include "Assets/partials/shop-banner.tpl"}
{include "Assets/partials/vote.tpl"}
{include "Assets/partials/menu.tpl"}
{include "Assets/partials/feedback.tpl"}
{if isset($error)}
	{if $error == 1}
		<div class="alert alert-success">
			<strong>Thành công!</strong> Đăng bình luận thành công!
		</div>
	{elseif $error == -1}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Nội dung bình luận không được để trống!
		</div>
	{/if}
{/if}
{if isset($username)}
  {include "Assets/partials/comment-box.tpl"}
{/if}
{/block}