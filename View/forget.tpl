{extends "Assets/partials/base_form.tpl"}
{block "title"}Quên mật khẩu{/block}
{block "noti"}
	{if $error == -1}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không tồn tại!
		</div>
	{elseif $error == -2}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Email không khớp với tài khoản
		</div>
	{elseif $error == -3}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Tên tài khoản không được để trống!
		</div>
	{elseif $error == -4}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Email không được để trống!
		</div>
	{elseif $error == 1}
		<div class="alert alert-success">
			<strong>Thành công!</strong> Mật khẩu đã được gửi qua email của bạn, lưu ý kiểm tra cả hộp thư rác!
		</div>
	{/if}
{/block}
{block "button-value"}Lấy lại mật khẩu{/block}
{block "username"}
    <div class="form-group">
      <label for="name">Tên đăng nhập:</label>
      <input type="text" class="form-control" id="name" placeholder="Nhập tài khoản đã đăng ký" name="username" required>
    </div>
{/block}
{block "email"}
	<div class="form-group">
      <label for="email">E-mail:</label>
      <input type="email" class="form-control" id="email" placeholder="Nhập email dùng để đăng ký" name="email" required>
    </div>
    <div class="help-block with-errors"></div>
{/block}
