{extends "Assets/partials/base_form.tpl"}
{block "title"}Đổi mật khẩu{/block}
{block "noti"}
	{if $error == -2}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Nhập lại mật khẩu không khớp!
		</div>
	{elseif $error == -3}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu mới phải trên 8 ký tự!
		</div>
	{elseif $error == 1}
		<div class="alert alert-success">
			<strong>Thành công!</strong> Đổi mật khẩu thành công!
		</div>
	{elseif $error == -1}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu hiện tại không đúng!
		</div>
	{elseif $error == -4}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu mói không được để trống!
		</div>
	{elseif $error == -5}
		<div class="alert alert-danger">
			<strong>Thất bại!</strong> Mật khẩu hiện tại không được để trống!
		</div>
	{/if}
{/block}
{block "button-value"}Xác nhận{/block}
{block "password"}
	<div class="form-group">
      <label for="pwd">Mật khẩu hiện tại:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Mật khẩu phải có ít nhất 8 ký tự" name="curpwd" required>
    </div>
    <div class="form-group">
      <label for="pwd">Mật khẩu mới:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Mật khẩu phải có ít nhất 8 ký tự" name="password" required>
    </div>
{/block}
{block "retype-password"}
<div class="form-group">
	<label for="pwd2">Nhập lại mật khẩu mới:</label>
	<input type="password" class="form-control" name="password2" required>
</div>
{/block}
